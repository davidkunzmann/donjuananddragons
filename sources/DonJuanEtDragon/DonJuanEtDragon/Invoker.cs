﻿using System;

namespace DonJuanEtDragon
{

	public interface Invoker
	{
		void setCommand(Command commande);
		void executeCommand();
	}


}

