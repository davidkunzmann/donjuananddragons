﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DonJuanEtDragon
{
	public abstract class Character
	{
		private int healthPoints;
		private int maxHealthPoints;

		private string name;

		private bool isAlive;

		private Texture2D sprite;

		public Character (string name, Texture2D sprite) {
			Initialize(name, sprite, 100);
		}

		public Character (string name, Texture2D sprite, int maxHP) {
			Initialize(name, sprite, maxHP);
		}
		private void Initialize( string name, Texture2D sprite, int maxHP){
			this.name = name;
			this.sprite = sprite;

			healthPoints = maxHealthPoints = maxHP;

			isAlive = true;
		}

		public int getHP() {
			return healthPoints;
		}

		public int getMaxHP() {
			return maxHealthPoints;
		}

		public void setMaxHP(int value) {
			maxHealthPoints = value;
		}

		public string getName ()
		{
			return name;
		}

		public void setSprite(Texture2D sprite) {
			this.sprite = sprite;
		}

		public Texture2D getSprite() {
			return sprite;
		}

		public bool takeDamages (int damages)
		{
			if (damages >= healthPoints) {
				healthPoints = 0;
				isAlive = false;
			} else {
				healthPoints -= damages;
			}

			return isAlive;
		}


	}
}

