﻿using System;

namespace DonJuanEtDragon
{
	public interface Command
	{
		void execute();
	}

}

