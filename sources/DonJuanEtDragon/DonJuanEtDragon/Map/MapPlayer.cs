﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace DonJuanEtDragon
{
	public class MapPlayer
	{
		private readonly Texture2D texture;
		private TileMap map;

		private int x, y;
		private int width, height;

		private int currentFrame;
		private int currentRowFrame;
		private int time;
		private const int animationSpeed = 11;

		public MapPlayer (TileMap map, Texture2D texture)
		{
			this.texture = texture;
			this.map = map;

			this.x = 100;
			this.y = 100;
			this.width = 28;
			this.height = 28;
		}

		public void Update()
		{
			int tmpX = 0;
			int tmpY = 0;
			int tmpX2 = 0;
			int tmpY2 = 0;

			int previousX = x;
			int previousY = y;

			if (Keyboard.GetState ().IsKeyDown (Keys.Up)) 
			{
				y--;
				currentRowFrame = 1;
				Animate ();

				tmpX = x / TileMap.TileSize;
				tmpY = y / TileMap.TileSize;

				tmpX2 = (x + width) / TileMap.TileSize;
				tmpY2 = y / TileMap.TileSize;
			}
			else if (Keyboard.GetState ().IsKeyDown (Keys.Down))
			{
				y++;
				currentRowFrame = 0;
				Animate ();

				tmpX = x / TileMap.TileSize;
				tmpY = (y + height) / TileMap.TileSize;

				tmpX2 = (x + width) / TileMap.TileSize;
				tmpY2 = (y + height) / TileMap.TileSize;
			}
			else if (Keyboard.GetState ().IsKeyDown (Keys.Right))
			{
				x++;
				currentRowFrame = 3;
				Animate ();

				tmpX = (x + width) / TileMap.TileSize;
				tmpY = y / TileMap.TileSize;

				tmpX2 = (x + width) / TileMap.TileSize;
				tmpY2 = (y + height) / TileMap.TileSize;
			}
			else if (Keyboard.GetState ().IsKeyDown (Keys.Left))
			{
				x--;
				currentRowFrame = 2;
				Animate ();

				tmpX = x / TileMap.TileSize;
				tmpY = y / TileMap.TileSize;

				tmpX2 = x / TileMap.TileSize;
				tmpY2 = (y + height) / TileMap.TileSize;
			}

			if (map.Map [tmpY, tmpX] == 30 || map.Map[tmpY2, tmpX2] == 30 
				|| map.Map [tmpY, tmpX] < 25 || map.Map[tmpY2, tmpX2] < 25
				|| map.Map [tmpY, tmpX] == 31 || map.Map[tmpY2, tmpX2] == 31) {
				x = previousX;
				y = previousY;
			}
		}

		public void Animate()
		{
			if (time == animationSpeed) 
			{
				currentFrame++;
				if (currentFrame * width >= texture.Width)
					currentFrame = 0;
				time = 0;
			}
			time++;
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw (texture, new Rectangle(x, y, width, height), new Rectangle(currentFrame * width, currentRowFrame * height, width, height), Color.White);
		}
	}
}

