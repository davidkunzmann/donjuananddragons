﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace DonJuanEtDragon
{
	public class TileMap
	{
		public static int TileSize = 32;

		private Texture2D tileset;

		public int[,] Map { get; set; } = {
			{22, 22, 22, 24, 25, 22, 22, 22, 22, 22, 23, 25, 22, 22, 25},
			{24,  7, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,  6, 22},
			{24, 11, 31, 29, 29, 29, 29, 28, 29, 29, 29, 29, 31, 12, 22},
			{22, 11, 29, 29, 29, 29, 29, 29, 29, 29, 28, 29, 29, 12, 22},
			{22,  9, 29, 29, 28, 29, 28, 29, 29, 29, 29, 29, 29,  8, 22},
			{23, 11, 29, 29, 27, 29, 29, 29, 29, 29, 29, 29, 29, 12, 24},
			{23, 9, 29, 29, 29, 29, 29, 29, 29, 29, 29, 28, 28, 8, 25},
			{22, 11, 26, 29, 29, 29, 29, 29, 29, 28, 29, 29, 27, 12, 25},
			{22, 9, 29, 29, 29, 29, 26, 29, 29, 29, 29, 29, 29, 8, 22},
			{22, 11, 29, 29, 29, 29, 29, 29, 29, 29, 29, 28, 29, 12, 24},
			{24,  9, 29, 27, 29, 29, 29, 29, 29, 29, 29, 27, 29,  8, 23},
			{25, 11, 29, 26, 29, 29, 29, 29, 29, 29, 29, 29, 29, 12, 23},
			{25, 11, 31, 29, 29, 29, 29, 26, 29, 29, 29, 29, 31, 12, 22},
			{22,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  5, 22},
			{22, 25, 24, 25, 22, 22, 22, 22, 23, 25, 24, 22, 22, 22, 24}
		};
		public int NbTilesX { get; set; } = 15;
		public int NbTilesY { get; set; } = 15;

		public TileMap (Texture2D tileset)
		{
			this.tileset = tileset;

			GenerateRandomMap ();
		}

		public void GenerateRandomMap()
		{
			Random rand = new Random ();
			for (int i = 2; i < NbTilesY - 2; i++) 
			{
				for (int j = 2; j < NbTilesX - 2; j++) 
				{
					if (Map [i, j] != 31) 
					{
						int randResult = rand.Next (26, 31);
						if (randResult == 30) 
						{
							if(rand.Next(2) == 0)
								Map [i, j] = randResult;
						} 
						else
							Map [i, j] = randResult;
					}
				}
			}
		}
			
		public void Draw(SpriteBatch spriteBatch)
		{
			for (int i = 0; i < NbTilesY; i++) 
			{
				for (int j = 0; j < NbTilesX; j++) 
				{
					int a = Map [j, i];
					Rectangle rectSource = new Rectangle (
						a % 10 * TileSize,
						a / 10 * TileSize,
						TileSize,
						TileSize);

					Rectangle rectDest = new Rectangle (i * TileSize, j * TileSize, TileSize, TileSize);

					spriteBatch.Draw (tileset, rectDest, rectSource, Color.White);

				}
			}
		}
	}
}

