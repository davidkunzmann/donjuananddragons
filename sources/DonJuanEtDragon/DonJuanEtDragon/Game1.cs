﻿#region Using Statements
using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using DonJuanEtDragon.Screens;

#endregion

namespace DonJuanEtDragon
{
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;

		Song song;

		TileMap tileMap;
		MapPlayer mapPlayer;
		battleScreen bScreen;
		ScreenManager screenManager;

		public Game1 ()
		{
			graphics = new GraphicsDeviceManager (this);
			Content.RootDirectory = "Content";

			graphics.PreferredBackBufferWidth = 480;
			graphics.PreferredBackBufferHeight = 480;
		}

		protected override void Initialize ()
		{
			base.Initialize ();


		}

		protected override void LoadContent ()
		{
			spriteBatch = new SpriteBatch (GraphicsDevice);

			tileMap = new TileMap (this, Content.Load<Texture2D> ("Textures/tileset"));

			bScreen = new battleScreen (this, Content);

			mapPlayer = new MapPlayer (this, tileMap, Content.Load<Texture2D> ("Textures/player"));

			screenManager = new ScreenManager (this, tileMap);
			screenManager.Push (mapPlayer);
			bScreen.setVisible (false);
			screenManager.Push (bScreen);

//			song = Content.Load<Song> ("Sounds/dungeon");
//			MediaPlayer.Play (song);
		}

		protected override void Update (GameTime gameTime)
		{
			#if !__IOS__
			if (GamePad.GetState (PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
			    Keyboard.GetState ().IsKeyDown (Keys.Escape)) {
				Exit ();
			}
			#endif

			mapPlayer.Update ();

			base.Update (gameTime);
		}

		protected override void Draw (GameTime gameTime)
		{
			graphics.GraphicsDevice.Clear (Color.CornflowerBlue);
		
			spriteBatch.Begin ();
				

			screenManager.Draw (gameTime, spriteBatch);

//			bScreen.Draw (gameTime, spriteBatch);
			if (!bScreen.isVisible () && Keyboard.GetState ().IsKeyDown (Keys.A)) {
				mapPlayer.setVisible (false);
				tileMap.setVisible (false);
				bScreen.setVisible (true);
			} else if (bScreen.isVisible () && Keyboard.GetState ().IsKeyDown (Keys.A)) {
				mapPlayer.setVisible (true);
				tileMap.setVisible (true);
				bScreen.setVisible (false);
			}
				

			screenManager.Draw (gameTime, spriteBatch);
//			tileMap.Draw (spriteBatch);
//
//			mapPlayer.Draw (spriteBatch);
//
			spriteBatch.End ();
            
			base.Draw (gameTime);
		}
	}
}
