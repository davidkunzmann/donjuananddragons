﻿#region Using Statements
using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

#endregion

namespace DonJuanEtDragon
{
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;

		Texture2D tileset;

		int[,] map = {
			{22, 22, 22, 24, 25, 22, 22, 22, 22, 22, 23, 25, 22, 22, 25},
			{24,  7, 10, 10, 10, 10, 18, 17, 16, 10, 10, 10, 10,  6, 22},
			{24, 11, 30, 29, 29, 29, 29, 28, 29, 29, 29, 29, 30, 12, 22},
			{22, 11, 29, 29, 29, 29, 29, 29, 29, 29, 28, 29, 29, 12, 22},
			{22,  9, 29, 29, 28, 29, 28, 29, 29, 29, 29, 29, 29,  8, 22},
			{23, 11, 29, 29, 27, 29, 29, 29, 29, 29, 29, 29, 29, 12, 24},
			{23, 15, 29, 29, 29, 29, 29, 31, 29, 29, 29, 28, 28, 21, 25},
			{22, 14, 26, 29, 29, 29, 31, 31, 31, 28, 29, 29, 27, 20, 25},
			{22, 13, 29, 29, 29, 29, 26, 31, 29, 29, 29, 29, 29, 19, 22},
			{22, 11, 29, 29, 29, 29, 29, 29, 29, 29, 29, 28, 29, 12, 24},
			{24,  9, 29, 27, 29, 29, 29, 29, 29, 29, 29, 27, 29,  8, 23},
			{25, 11, 29, 26, 29, 29, 29, 29, 29, 29, 29, 29, 29, 12, 23},
			{25, 11, 30, 29, 29, 29, 29, 26, 29, 29, 29, 29, 30, 12, 22},
			{22,  0,  1,  1,  1,  1,  2,  3,  4,  1,  1,  1,  1,  5, 22},
			{22, 25, 24, 25, 22, 22, 22, 22, 23, 25, 24, 22, 22, 22, 24}
		};

		Song song;

		public Game1 ()
		{
			graphics = new GraphicsDeviceManager (this);
			Content.RootDirectory = "Content";

			graphics.PreferredBackBufferWidth = 480;
			graphics.PreferredBackBufferHeight = 480;
		}

		protected override void Initialize ()
		{
			base.Initialize ();
				
		}

		protected override void LoadContent ()
		{
			spriteBatch = new SpriteBatch (GraphicsDevice);

			tileset = Content.Load<Texture2D> ("Textures/tileset");

			//*
			song = Content.Load<Song> ("Sounds/dungeon");
			MediaPlayer.Play (song);
			//*/
		}

		protected override void Update (GameTime gameTime)
		{
			#if !__IOS__
			if (GamePad.GetState (PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
			    Keyboard.GetState ().IsKeyDown (Keys.Escape)) {
				Exit ();
			}
			#endif



			base.Update (gameTime);
		}

		protected override void Draw (GameTime gameTime)
		{
			graphics.GraphicsDevice.Clear (Color.CornflowerBlue);
		
			spriteBatch.Begin ();

			for (int i = 0; i < 15; i++) 
			{
				for (int j = 0; j < 15; j++) 
				{
					int a = map [j, i];
					Rectangle rectSource = new Rectangle (
						a % 10 * 32,
						a / 10 * 32,
                        32,
                        32);

					Rectangle rectDest = new Rectangle (i * 32, j * 32, 32, 32);

					spriteBatch.Draw (tileset, rectDest, rectSource, Color.White);

				}
			}



			spriteBatch.End ();
            
			base.Draw (gameTime);
		}
	}
}

